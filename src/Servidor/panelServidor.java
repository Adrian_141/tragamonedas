package Servidor;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;

public class panelServidor extends javax.swing.JPanel {
    private Connection conexion;
    private panelServidor panel;
    private final int PORT = 1234;
        
    public panelServidor() {
        try {
            initComponents();
            new Hilo().start();
            botonSalir.addActionListener((ActionEvent e) -> {
                System.exit(0);
            });
            Class.forName("com.mysql.cj.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://localhost/tragamonedas?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
        } catch (ClassNotFoundException ex) {
        } catch (SQLException ex) {
            
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelTexto = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        panelBotones = new javax.swing.JPanel();
        botonSalir = new javax.swing.JButton();
        panelTabla = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaUsuarios = new javax.swing.JTable();

        setLayout(new java.awt.BorderLayout());

        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 15)); // NOI18N
        jLabel1.setText("Panel de Control de Usuarios");
        panelTexto.add(jLabel1);

        add(panelTexto, java.awt.BorderLayout.NORTH);

        panelBotones.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 15, 5));

        botonSalir.setText("Salir");
        botonSalir.setName("salir"); // NOI18N
        botonSalir.setPreferredSize(new java.awt.Dimension(100, 23));
        panelBotones.add(botonSalir);

        add(panelBotones, java.awt.BorderLayout.SOUTH);

        tablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nombre", "Créditos", "Juegos", "Ganados", "Perdidos"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });

        jScrollPane1.setPreferredSize(new Dimension(450, 230));
        panelTabla.setPreferredSize(new Dimension(450, 230));
        tablaUsuarios.setPreferredSize(new Dimension(450, 230));
        
        jScrollPane1.setViewportView(tablaUsuarios);

        panelTabla.add(jScrollPane1);

        add(panelTabla, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel panelBotones;
    private javax.swing.JPanel panelTabla;
    private javax.swing.JPanel panelTexto;
    private javax.swing.JTable tablaUsuarios;
    // End of variables declaration//GEN-END:variables

    /**
     * @return the tablaUsuarios
     */
    public javax.swing.JTable getTablaUsuarios() {
        return tablaUsuarios;
    }
    public class Hilo extends Thread{
        private DataInputStream entrada;
        private DataOutputStream salida;
        private String line [];
        
        @Override
        public void run(){
            try {
                ServerSocket servidor = new ServerSocket(PORT);
                Socket socket = null;
                for(;;){
                    socket = servidor.accept();
                    entrada = new DataInputStream(socket.getInputStream());
                    salida = new DataOutputStream(socket.getOutputStream());
                    String mensaje = entrada.readUTF();
                    line = mensaje.split(" ");
                    if(line[0].equals("1")) sesion();
                    else if(line[0].equals("2")) tabla();
                    else if(line[0].equals("3")) creditos();
                    else if(line[0].equals("4")) juegos();
                    else if(line[0].equals("5")) crear();
                    socket.close();
                }
            } catch (IOException ex) {}
              catch (SQLException ex) {}

        }
        
        private void sesion() throws IOException {
            try {
                String consulta = "SELECT count(idJugador) FROM jugador WHERE usuario=\"" + line[1] + "\"";
                Statement query = conexion.createStatement();
                ResultSet resultado = query.executeQuery(consulta);
                int numRows = 0;
                if (resultado.next()) {
                    numRows = resultado.getInt(1);
                }
                
                if (numRows == 1) {
                    consulta = "SELECT password FROM jugador WHERE usuario=\"" + line[1] + "\"";
                    resultado = query.executeQuery(consulta);
                    String password = "";
                    if (resultado.next()) {
                        password = resultado.getString(1);
                    }

                    if (line[2].equals(password)) {
                        salida.writeUTF("OK");
                    } else {
                        salida.writeUTF("BadPassword");
                    }
                } else {
                    salida.writeUTF("NotFound");
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        private void tabla() throws IOException, SQLException {
            String query;
            Statement st;
            ResultSet rs;
            DefaultTableModel modelo = (DefaultTableModel) getTablaUsuarios().getModel();
            Object[] row = new Object[5];
            query = "SELECT usuario, creditos, juegos, ganados, perdidos FROM cuenta, jugador WHERE usuario = \""
                    + line[1] + "\" AND jugador.idCuenta = cuenta.idCuenta";
            st = conexion.createStatement();
            rs = st.executeQuery(query);
            if (rs.next()) {
                for (int i = 0; i < 5; i++) {
                    row[i] = rs.getObject(i + 1);
                }
            }
            salida.writeUTF(Integer.toString((Integer) row[1]));
            modelo.addRow(row);
            getTablaUsuarios().setModel(modelo);

                
        }
        
        private void creditos() {
            String query = "UPDATE cuenta SET creditos = " + line[2] + " WHERE idCuenta = "
                    + "(SELECT idCuenta FROM jugador WHERE usuario = \""
                    + line[1] + "\");";
            Statement st;
            try {
                st = conexion.createStatement();
                int r = st.executeUpdate(query);
                DefaultTableModel modelo = (DefaultTableModel) getTablaUsuarios().getModel();
                int rows = modelo.getRowCount();
                for (int i = 0; i < rows; i++) {
                    String nombre = (String) modelo.getValueAt(i, 0);
                    if (nombre.equals(line[1])) {
                        modelo.setValueAt(line[2], i, 1);
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        private void juegos() {
            String query;
            if (line[3].equals("Win")) {
                query = "UPDATE cuenta SET creditos = " + line[2] + ", ganados = ganados + 1, juegos = juegos + 1  WHERE idCuenta = "
                        + "(SELECT idCuenta FROM jugador WHERE usuario = \""
                        + line[1] + "\");";
            } else {
                query = "UPDATE cuenta SET creditos = " + line[2] + ", perdidos = perdidos + 1, juegos = juegos + 1 WHERE idCuenta = "
                        + "(SELECT idCuenta FROM jugador WHERE usuario = \""
                        + line[1] + "\");";
            }
            try {
                Statement st = conexion.createStatement();
                int r = st.executeUpdate(query);
                DefaultTableModel modelo = (DefaultTableModel) getTablaUsuarios().getModel();
                int rows = modelo.getRowCount();
                for (int i = 0; i < rows; i++) {
                    String nombre = (String) modelo.getValueAt(i, 0);
                    int win = (int) modelo.getValueAt(i, 3);
                    int loss = (int) modelo.getValueAt(i, 4);
                    int juegos = (int) modelo.getValueAt(i, 2);
                    if (nombre.equals(line[1])) {
                        modelo.setValueAt(line[2], i, 1);
                        juegos++;
                        modelo.setValueAt(juegos, i, 2);
                        if (line[3].equals("Win")) {
                            win++;
                            modelo.setValueAt(win, i, 3);
                        } else {
                            loss++;
                            modelo.setValueAt(loss, i, 4);
                        }
                    }
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        private void crear() throws IOException{
            try {
                String consulta = "SELECT count(idJugador) FROM jugador WHERE usuario=\"" + line[1] + "\"";
                Statement query = conexion.createStatement();
                ResultSet resultado = query.executeQuery(consulta);
                int numRows = 0;
                if (resultado.next()) 
                    numRows = resultado.getInt(1);
                
                // Si el usuario existe
                if (numRows == 1) {
                    salida.writeUTF("Existe");
                } else {
                    
                    int r;
                    
                    consulta = "INSERT INTO cuenta(creditos, juegos, ganados, perdidos) VALUES(0, 0, 0, 0)";
                    query = conexion.createStatement();
                    r = query.executeUpdate(consulta);
                    
                    consulta = "SELECT idCuenta FROM cuenta;";
                    query = conexion.createStatement();
                    resultado = query.executeQuery(consulta);
                    int idCuenta = 0;
                    resultado.afterLast();
                    if(resultado.previous())
                        idCuenta = resultado.getInt(1);
                    consulta = "INSERT INTO jugador(usuario, password, idCuenta) VALUES(\"" +line[1]+"\", \""+ line[2]+"\", " +idCuenta +")";
                    r = query.executeUpdate(consulta);
                    if(r >= 1) 
                        salida.writeUTF("OK");
                    
                    
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
    }
}
