package login;

import javax.swing.*;

public class Main {
    
    public static void main(String args[]){
        JFrame frame = new JFrame();
        Login login = new Login(frame);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(100, 100);
        frame.setSize(380, 200);
        frame.add(login);
        frame.setVisible(true);
    }    
    
}
